#ifndef MODEL_FACTORY_HPP
#define MODEL_FACTORY_HPP


#include <iostream>
#include <vector>
#include <map>
#include <stb_image.h>
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include "Mesh.hpp"
#include "Model.hpp"
#include "TextureLoader.hpp"

class ModelFactory
{
public:

	ModelFactory();
	~ModelFactory();
	bool preloadModel(std::string const & path);
	Model* loadModel(std::string const & path);
	Texture loadTexture(std::string const & path);
private:
	std::string directory;
	std::vector<Texture> loadedTextures; // To avoid texture loading duplication
	std::map<std::string, std::vector<Mesh>> loadedModels;

    std::vector<Mesh> processNode(aiNode* node, const aiScene* scene);
    Mesh processMesh(aiMesh* mesh, const aiScene* scene);
	std::vector<Texture> loadMaterialTexture(aiMaterial* material, aiTextureType type, std::string typeName);

};

#endif