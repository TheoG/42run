#ifndef TERRAIN_GENERATOR_HPP
#define TERRAIN_GENERATOR_HPP

#include <glm/glm.hpp>
#include <list>

#include "ModelFactory.hpp"
#include "ResourceManager.hpp"

#define TILE_SIZE 6.0f

class TerrainGenerator {

public:

	TerrainGenerator(ModelFactory&, ResourceManager&, std::list<Model*>&, std::list<Model*>&, std::list<Model*>&);
	~TerrainGenerator();

	const glm::ivec3& getLastBranchPosition();

	void generatePath(glm::ivec3 direction);
	void setDirection(glm::ivec3 direction);

	void setDifficulty(int difficulty);
	void reset();

private:
	ModelFactory& modelFactory;
	ResourceManager& resourceManager;

	std::list<Model*>& sceneries;
	std::list<Model*>& collidables;
	std::list<Model*>& bonuses;

	glm::ivec3 lastBranchPosition;
	glm::ivec3 direction;

	int distanceBeforeBranch;

	int difficulty;

	void generateCollidables();
	void generateBranch();
	void generateBonuses();

	bool checkCollidablePosition(Model* collidable);
};

#endif