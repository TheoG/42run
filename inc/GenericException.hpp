#ifndef GENERIC_EXCEPTION_HPP
#define GENERIC_EXCEPTION_HPP

#include <string>
#include <exception>

class GenericException: public std::exception
{
public:
	GenericException(const std::string& message);
	virtual const char* what() const throw();
private:
	std::string message;
};

#endif