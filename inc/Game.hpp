#ifndef GAME_HPP
#define GAME_HPP

#include <list>

#include "Player.hpp"
#include "Model.hpp"
#include "ModelFactory.hpp"
#include "ResourceManager.hpp"
#include "HUD.hpp"
#include "Camera.hpp"
#include "TerrainGenerator.hpp"

#define DEFAULT_WIDTH 1280
#define DEFAULT_HEIGHT 720

class Game {

public:

	Game(ModelFactory& modelFactory, ResourceManager& resourceManager);
	~Game();


	void processInput(GLFWwindow* winPtr, float deltaTime);
	void update(float deltaTime);
	void render(Shader* shader, Shader* printerShader, Shader* textureShader, float deltaTime);
	void renderMainMenu(Shader* shader, Shader* printerShader, Shader* textureShader, float deltaTime);
	void renderRetryMenu(Shader* shader, Shader* printerShader, Shader* textureShader, float deltaTime);

	void rotate(float angle);

	void resize(int width, int height);
	Player* getPlayer() const;

	void setWindow(Window* window);
	Camera& getCamera();

	bool isPaused() const;
	void setPause(bool pause);

	enum State {
		MAIN_MENU,
		PLAY,
		PAUSE,
		RETRY,
		END
	};

	State getState() const;
	short getMainMenuPosition() const;
	void setState(State state);
	void increaseMainMenuPosition();
	void decreaseMainMenuPosition();

	short getRetryMenuPosition() const;
	void increaseRetryMenuPosition();
	void decreaseRetryMenuPosition();

	void reset();

private:

	Player* player;
	ModelFactory& modelFactory;
	ResourceManager& resourceManager;
	Camera camera;
	HUD* hud;
	TerrainGenerator* generator;

	glm::ivec2 windowSize;

	glm::vec3 boundaries;
	float remainingRotation;

	bool paused;

	short mainMenuPosition;
	short retryMenuPosition;

	std::list<Model*> collidables;
	std::list<Model*> sceneries;
	std::list<Model*> bonuses;

	State state;

	void setupPlayer();
	void updatePlayer(float deltaTime);
	void renderPlayer(Shader* shader, float deltaTime);

	void updateEntities(float deltaTime);
	void renderEntities(Shader* shader, float deltaTime);

	void updateBonuses(float deltaTime);
	void renderBonuses(Shader* shader, float deltaTime);

	void updateCamera(float deltaTime);

	void renderHUD(Shader* textShader, Shader* textureShader);
};

#endif