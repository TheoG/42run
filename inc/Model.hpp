#ifndef MODEL_HPP
#define MODEL_HPP

#include <iostream>
#include <vector>

#include <stb_image.h>

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include "Mesh.hpp"

class Model
{
public:
	Model(std::vector<Mesh> meshes);
	Model(Model& other);
	virtual ~Model();

    void draw(Shader* shader, float deltaTime);
	bool isCollision(Model* other);

	void move(glm::vec3 vector);
	virtual void rotate(float angle, glm::vec3 vector);
	void resize(glm::vec3 vector);

	void generateAABB3DValues();

	void setPosition(glm::vec3 vector);
	void setTranslation(glm::vec3 vector);
	void setRotation(glm::vec3 vector);
	void setVelocity(glm::vec3 velocity);
	void setSize(glm::vec3 size);

	const glm::mat4& getModelMatrix() const;
	const glm::vec3& getVelocity() const;
	const glm::vec3& getPosition() const;
	const glm::vec3& getRotation() const;
	const glm::mat4& getRotationMatrix() const;
	std::vector<Mesh>& getMeshes();

	static const float gravity;

	enum Type {
		DEFAULT,
		PLAYER,
		COIN,
		WALL,
		HEART,
		NEON,
		AUTOLAVEUSE,
		TRASHES
	};

	void setType(Type type);
	Type getType() const;

private:
	std::vector<Mesh> meshes;
protected:
	void updateModelMatrix();

	glm::vec3 velocity;

	glm::vec3 position;
	glm::vec3 rotation;
	glm::vec3 size;

	glm::mat4 rotationMatrix;
	glm::mat4 modelMatrix;

	glm::vec3 aabbPosition;
	glm::vec3 aabbSize;
	glm::vec3 worldToLocalPosition;

	Type type;
};

#endif