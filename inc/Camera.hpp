#ifndef CAMERA_HPP
#define CAMERA_HPP

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <iostream>

class Window;

#define CAMERA_POS_Y 3.0f
#define CAMERA_POS_Z 8.0f

class Camera
{
public:
	Camera();
	Camera(Window* w);
	~Camera();

	const glm::mat4& getViewMatrix() const;
	const glm::mat4& getProjectionMatrix() const;
	glm::vec3 getPosition() const;

	void setFront(glm::vec3 front);
	void setWindow(Window* window);
	void setProjectionSettings(float fov, int width, int height, float near, float far);

	void setLookAt(glm::vec3 lookAt);
	void setPosition(glm::vec3 position);
	void move(glm::vec3 shift);
	void reset();
private:

	Window* window;

	glm::vec3 position;
	glm::vec3 front;
	glm::vec3 up;
	glm::vec3 lookAt;

	glm::mat4 view;
	glm::mat4 projection;
};

#include "Window.hpp"

#endif