#ifndef WINDOW_HPP
#define WINDOW_HPP

#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <list>

#include "ModelFactory.hpp"
#include "Player.hpp"
#include "Shader.hpp"
#include "ResourceManager.hpp"
#include "Camera.hpp"

class Game;

class Window
{
public:
	Window(int width, int height);
	~Window();
	void display();
	void processInput(float deltaTime);
	void updateWindowTitle();


	void resize(int width, int height);

	GLFWwindow* getWindow() const;
	Game*	getGame() const;

	int width;
	int height;

private:

	float deltaTime;

	GLFWwindow*	window;
	Shader* s;
	Shader* printerShader;
	Shader* textureShader;
	ResourceManager resourceManager;
	ModelFactory modelFactory;
	Game* game;
};

#include "Game.hpp"


#endif