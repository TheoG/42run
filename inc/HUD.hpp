#ifndef HUD_HPP
#define HUD_HPP

#include <ft2build.h>
#include FT_FREETYPE_H

#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <iostream>
#include <map>

#include "Shader.hpp"
#include "TextureLoader.hpp"
#include "ResourceManager.hpp"

struct PrintableCharacter {
	GLuint textureID;
	glm::ivec2 size;
	glm::ivec2 bearing;
	GLuint advance;
};

class HUD {

public:

	HUD(const std::string& fontPath);
	~HUD();

	void addElement(const std::string& texturePath);

	void setWindowSize(int width, int height);
	void renderText(Shader* s, const std::string& text, GLfloat x, GLfloat y, GLfloat scale, glm::vec3 color);
	
	void renderElement(Shader* s, const std::string& elementName, GLfloat x, GLfloat y, GLfloat scaleX, GLfloat scaleY);

	
private:
	std::map<GLchar, PrintableCharacter> characters;
	std::map<std::string, GLuint> textures;
	glm::mat4 projection;
	GLuint VAO;
	GLuint VBO;

};

#endif