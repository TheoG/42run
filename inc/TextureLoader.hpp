#ifndef TEXTURE_LOADER_HPP
#define TEXTURE_LOADER_HPP

#include <stb_image.h>
#include <string>
#include <iostream>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

class TextureLoader
{
public:
	TextureLoader();
	~TextureLoader();

	static unsigned int textureFromFile(const std::string& path);
};

#endif