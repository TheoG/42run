#ifndef MESH_HPP
#define MESH_HPP

#include <glm/glm.hpp>
#include <vector>
#include <iostream>

#include "Shader.hpp"

struct Vertex {
	glm::vec3 position;
	glm::vec3 normal;
	glm::vec2 textureCoordinates;
};

struct Texture {
	unsigned int id;
	std::string type;
	std::string path;
};

class Mesh
{
public:
	Mesh(std::vector<Vertex> vertices, std::vector<unsigned int> indices, std::vector<Texture> textures);
	~Mesh();

	void draw(Shader* shader, float deltaTime);
	void addTexture(Texture texture);

	void setAnimationDelay(float delay);

	const std::vector<Vertex>& getVertices() const;

private:
	unsigned int VAO;
	unsigned int VBO;
	unsigned int EBO;

	float animationDelay;
	float textureUpdateCounter;
	int currentFrame;

	std::vector<Vertex> vertices;
	std::vector<unsigned int> indices;
	std::vector<Texture> textures;

	void setup();
};

#endif