#ifndef FILE_EXCEPTON_HPP
#define FILE_EXCEPTON_HPP

#include <string>
#include <exception>

class FileException: public std::exception
{
public:

	enum Type {
		DEFAULT,
		TEXTURE,
		FONT,
		SHADER,
		SHADER_ERROR,
		NOT_FOUND
	};

	FileException(const std::string& fileName, Type type);
	virtual const char* what() const throw();
private:
	std::string fileName;
	Type type;
};

#endif