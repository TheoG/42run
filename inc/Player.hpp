#ifndef PLAYER_HPP
#define PLAYER_HPP

#include "Model.hpp"

class Player: public Model
{
public:

	Player(std::vector<Mesh> meshes);
	Player(Model& other);
	~Player();
	void reset();

	enum Direction {
		NORTH,
		WEST,
		SOUTH,
		EAST,
	};

	enum Damage {
		COLLIDABLE,
		WALL
	};

	void processInput(GLFWwindow* winPtr, float deltaTime);
	void crouch();
	void straighten();
	
	void takeDamage(Damage damageType, short damage);
	void heal(short hp);


	void rotate(float angle, glm::vec3 axis);
	void setBoundaries(glm::vec3 boundaries);
	void setScore(float score);
	void setCrouchTime(float time);
	void decreaseDamageCooldown(float delta);
	void increaseScore(float score);
	void increaseDistance(float distance);
	void increaseCrouchTime(float delta);
	void increaseNbCoin(void);


	const glm::vec3& getBoundaries() const;
	Damage	getDamageType() const;
	Direction getDirection() const;
	float	getScore() const;
	float	getDamageCooldown() const;
	float 	getCrouchTime() const;
	float	getDistance() const;
	short	getLife() const;
	bool	hasToBlink();
	bool	isCrouched();
	int		getNbCoins() const;


	static const float	defaultDamageCooldown;
	static const float	crouchDuration;
	static const int	nbFrameTextureAnimation;

private:

	glm::vec3	boundaries;
	Damage		damageType;
	Direction	direction;
	float		score;
	float		distance;
	float		damageCooldown;
	float		crouchTime;
	int			nbCoins;
	int			life;

};

#endif
