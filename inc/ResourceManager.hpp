#ifndef RESOURCE_MANAGER_HPP
#define RESOURCE_MANAGER_HPP

#include <iostream>

class ResourceManager
{
public:
	ResourceManager();
	~ResourceManager();

	void setResourcePath(const std::string& resourcePath);
	
	std::string getModelPath(const std::string& modelName);
	std::string getShaderPath(const std::string& shaderName);
	std::string getFontPath(const std::string& fontName);
	const std::string& getResourcePath();

	static const std::string shaderFolder;
	static const std::string modelFolder;
	static const std::string fontFolder;
private:
	std::string resourcePath;
};

#endif