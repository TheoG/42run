#ifndef SHADER_H
# define SHADER_H

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

# include <string>
# include <fstream>
# include <sstream>
# include <iostream>

class Shader {

	public:
		Shader(const std::string& vertexPath, const std::string& fragmentPath);
		Shader();
		Shader(Shader const &);
		~Shader(void);
		Shader & operator=(Shader const & rhs);
		void use() const;


		void setBool(const std::string &name, bool value) const;
		void setInt(const std::string &name, int value) const;
		void setFloat(const std::string &name, float value) const;
		void setVec3f(const std::string &name, glm::vec3 value) const;
		void setMatrix(const std::string &name, glm::mat4 value) const;

	private:
		unsigned int ID;
		void checkCompileErrors(unsigned int shader, std::string type);
	};

#endif