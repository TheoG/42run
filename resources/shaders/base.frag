#version 410 core

out vec4 FragColor;

in vec2 TexCoords;

uniform sampler2D texture_diffuse1;
uniform bool		isHurt;

void main()
{
	vec4 redColor = vec4(1.0, 0.0, 0.0, 1.0);
	FragColor = texture(texture_diffuse1, TexCoords);
	if (isHurt) {
		FragColor.x = 1.0;
	}
}