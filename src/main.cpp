#include <iostream>
#include <exception>

#include "Window.hpp"
#include "ResourceManager.hpp"

int main() {
	try {
		Window* win = new Window(DEFAULT_WIDTH, DEFAULT_HEIGHT);
		win->display();
		delete win;

	} catch(std::exception &e) {
		std::cerr << e.what() << std::endl;
	}
	return (0);
}