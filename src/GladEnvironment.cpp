#include "GladEnvironment.hpp"

GladEnvironment::GladEnvironment() {
	typedef void (*GL_GENBUFFERS) (GLsizei, GLuint*);
	GL_GENBUFFERS glGenBuffers  = (GL_GENBUFFERS)glfwGetProcAddress("glGenBuffers");
	(void)glGenBuffers;
}

GladEnvironment::~GladEnvironment() {}