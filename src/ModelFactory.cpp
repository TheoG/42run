#include "ModelFactory.hpp"
#include "FileException.hpp"
#include "GenericException.hpp"

ModelFactory::ModelFactory() {}
ModelFactory::~ModelFactory() {}

Model* ModelFactory::loadModel(std::string const &path) {
	
	Model* model = nullptr;

	if ( this->loadedModels.find(path) != this->loadedModels.end() ) {
		model = new Model(this->loadedModels[path]);
	}
	else if (this->preloadModel(path)) {
		model = new Model(this->loadedModels[path]);
	}
	if (!model) {
		throw GenericException("Model loading error");
	}
	return model;
}

bool ModelFactory::preloadModel(const std::string& path) {
	if (this->loadedModels.find(path) != this->loadedModels.end()) {
		return true;
	}

	Assimp::Importer importer;
	const aiScene* scene = importer.ReadFile(path, aiProcess_Triangulate | aiProcess_FlipUVs | aiProcess_CalcTangentSpace | aiProcess_GenNormals);

	if (!scene || scene->mFlags & AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode) {
		throw FileException(path, FileException::Type::DEFAULT);
	}
	directory = path.substr(0, path.find_last_of('/'));

	std::vector<Mesh> createdModel = processNode(scene->mRootNode, scene);
	this->loadedModels[path] = createdModel;
	return true;
}

std::vector<Mesh> ModelFactory::processNode(aiNode *node, const aiScene *scene) {
	std::vector<Mesh> meshes;
	for (unsigned int i = 0; i < node->mNumMeshes; i++) {
		aiMesh* mesh = scene->mMeshes[node->mMeshes[i]];
		meshes.push_back(processMesh(mesh, scene));
	}
	for (unsigned int i = 0; i < node->mNumChildren; i++) {
		std::vector<Mesh> childMeshes = processNode(node->mChildren[i], scene);
		meshes.insert(meshes.end(), childMeshes.begin(), childMeshes.end());
	}

	return meshes;
}

Mesh ModelFactory::processMesh(aiMesh* mesh, const aiScene* scene) {

	std::vector<Vertex> vertices;
	std::vector<unsigned int> indices;
	std::vector<Texture> textures;

	for (unsigned int i = 0; i < mesh->mNumVertices; i++) {
		Vertex vertex;
		glm::vec3 vector;

		vector.x = mesh->mVertices[i].x;
		vector.y = mesh->mVertices[i].y;
		vector.z = mesh->mVertices[i].z;
		vertex.position = vector;


		vector.x = mesh->mNormals[i].x;
		vector.y = mesh->mNormals[i].y;
		vector.z = mesh->mNormals[i].z;
		vertex.normal = vector;


		if (mesh->mTextureCoords[0]) {
			glm::vec2 vector2;
			vector2.x = mesh->mTextureCoords[0][i].x;
			vector2.y = mesh->mTextureCoords[0][i].y;
			vertex.textureCoordinates = vector2;
		} else {
			vertex.textureCoordinates = glm::vec2(0.f, 0.f);
		}

		vertices.push_back(vertex);
	}
	for (unsigned int i = 0; i < mesh->mNumFaces; i++) {
		aiFace face = mesh->mFaces[i];
		for (unsigned int j = 0; j < face.mNumIndices; j++) {
			indices.push_back(face.mIndices[j]);
		}
	}

	if ((int)mesh->mMaterialIndex >= 0) {
		aiMaterial* material = scene->mMaterials[mesh->mMaterialIndex];

		std::vector<Texture> diffuseMaps = this->loadMaterialTexture(material, aiTextureType_DIFFUSE, "texture_diffuse");
		textures.insert(textures.end(), diffuseMaps.begin(), diffuseMaps.end());

		std::vector<Texture> specularMaps = this->loadMaterialTexture(material, aiTextureType_SPECULAR, "texture_specular");
		textures.insert(textures.end(), specularMaps.begin(), specularMaps.end());

	}

	return Mesh(vertices, indices, textures);
}


std::vector<Texture> ModelFactory::loadMaterialTexture(aiMaterial* material, aiTextureType type, std::string typeName) {
	
	std::vector<Texture> textures;
	for (unsigned int i = 0; i < material->GetTextureCount(type); i++) {
		aiString str;
		material->GetTexture(type, i, &str);
		bool skip = false;

		for (unsigned int j = 0; j < this->loadedTextures.size(); j++) {
			if (std::strcmp(this->loadedTextures[j].path.data(), str.C_Str()) == 0) {
				textures.push_back(this->loadedTextures[j]);
				skip = true;
				break;
			}
		}
		if (!skip) {
			Texture texture;
			texture.id = TextureLoader::textureFromFile(this->directory + "/" + std::string(str.C_Str()));
			texture.type = typeName;
			texture.path = str.C_Str();
			textures.push_back(texture);
			this->loadedTextures.push_back(texture);
		}
	}
	return textures;
}

Texture ModelFactory::loadTexture(std::string const & path) {
	Texture newTexture;
	
	newTexture.id = TextureLoader::textureFromFile(this->directory + "/" + path);
	newTexture.path = path;
	newTexture.type = "texture_diffuse";
	this->loadedTextures.push_back(newTexture);
	return newTexture;
}