#include "Camera.hpp"

Camera::Camera(): Camera::Camera(nullptr) {}

Camera::Camera(Window *window): window(window) {
	this->reset();
	this->projection = glm::perspective(glm::radians(45.0f), (float)DEFAULT_WIDTH / (float)DEFAULT_HEIGHT, 0.1f, 1000.0f);
}

void Camera::reset() {
	this->position	= glm::vec3(0.0f, CAMERA_POS_Y, CAMERA_POS_Z);
	this->front		= glm::vec3(0.0f,  0.0f,  0.0f);
	this->up		= glm::vec3(0.0f,  1.0f,  0.0f);
	this->lookAt	= glm::vec3(0.0f,  0.0f,  0.0f);
	this->view		= glm::lookAt(this->position, this->position + this->front, this->up);
}

Camera::~Camera() {}

const glm::mat4& Camera::getViewMatrix() const {
	return this->view;
}

const glm::mat4& Camera::getProjectionMatrix() const {
	return this->projection;
}

glm::vec3 Camera::getPosition() const {
	return this->position;
}

void Camera::setProjectionSettings(float fov, int width, int height, float nearCam, float farCam) {
	this->projection = glm::perspective(glm::radians(fov), static_cast<float>(width) / static_cast<float>(height), nearCam, farCam);
}

void Camera::setFront(glm::vec3 front) {
	this->front = front;
	this->view  = glm::lookAt(this->position, this->position + this->front, this->up);
}

void Camera::setWindow(Window* window) {
	this->window = window;
}

void Camera::setLookAt(glm::vec3 lookAt) {
	this->lookAt = lookAt;
	this->view = glm::lookAt(this->position, this->lookAt, this->up);
}

void Camera::move(glm::vec3 shift) {
	this->position += shift;
	this->view = glm::lookAt(this->position, this->lookAt, this->up);
}

void Camera::setPosition(glm::vec3 position) {
	this->position = position;
	this->view = glm::lookAt(this->position, this->lookAt, this->up);
}