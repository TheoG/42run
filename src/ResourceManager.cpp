#include "ResourceManager.hpp"

ResourceManager::ResourceManager() {}

ResourceManager::~ResourceManager() {}

void ResourceManager::setResourcePath(const std::string& resourcePath) {
	this->resourcePath = resourcePath;


#ifdef _WIN32
	this->resourcePath = this->resourcePath.substr(0, this->resourcePath.size() - 6) + "/resources/";
#endif
	
	if (this->resourcePath[this->resourcePath.size() - 1] != '/') {
		this->resourcePath += '/';
	}
}

std::string ResourceManager::getModelPath(const std::string& modelName) {
	return this->resourcePath + ResourceManager::modelFolder + modelName;
}

std::string ResourceManager::getFontPath(const std::string& fontName) {
	return this->resourcePath + ResourceManager::fontFolder + fontName;
}

std::string ResourceManager::getShaderPath(const std::string& shaderName) {
	return this->resourcePath + ResourceManager::shaderFolder + shaderName;
}

const std::string& ResourceManager::getResourcePath() {
	return this->resourcePath;
}

const std::string ResourceManager::shaderFolder = "shaders/";
const std::string ResourceManager::modelFolder = "models/";
const std::string ResourceManager::fontFolder = "fonts/";