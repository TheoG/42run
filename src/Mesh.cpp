#include "Mesh.hpp"

#include <glm/gtc/matrix_transform.hpp>

Mesh::Mesh(std::vector<Vertex> vertices, std::vector<unsigned int> indices, std::vector<Texture> textures) {
	this->vertices	= vertices;
	this->indices	= indices;
	this->textures	= textures;

	this->currentFrame			= 0;
	this->textureUpdateCounter	= 0.0f;
	this->animationDelay		= 0.07f;

	this->setup();
}

Mesh::~Mesh() {}

void Mesh::setup() {
	glGenVertexArrays(1, &this->VAO);
	glGenBuffers(1, &this->VBO);
	glGenBuffers(1, &this->EBO);

	glBindVertexArray(this->VAO);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);

	glBufferData(GL_ARRAY_BUFFER, this->vertices.size() * sizeof(Vertex), &this->vertices[0], GL_STATIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, this->indices.size() * sizeof(unsigned int), &this->indices[0], GL_STATIC_DRAW);

	// Position
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)0);
	
	// Normal
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)(sizeof(glm::vec3)));
	
	// Texture Coordinates
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)(sizeof(glm::vec3) + sizeof(glm::vec3)));

	glBindVertexArray(0);
}

void Mesh::draw(Shader* shader, float deltaTime) {
	(void)shader;

	if (this->textures.size()) {
		this->textureUpdateCounter += deltaTime;

		if (this->textureUpdateCounter > this->animationDelay) {
			this->currentFrame++;
			this->currentFrame %= this->textures.size();
			this->textureUpdateCounter = 0;
		}

		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, this->textures[this->currentFrame].id);
	} else {
		glBindTexture(GL_TEXTURE_2D, 0);
	}

	// Finally draw the mesh
	glBindVertexArray(this->VAO);
	glDrawElements(GL_TRIANGLES, this->indices.size(), GL_UNSIGNED_INT, 0);
	glBindVertexArray(0);
}

void Mesh::addTexture(Texture texture) {
	this->textures.push_back(texture);
}

void Mesh::setAnimationDelay(float delay) {
	this->animationDelay = delay;
}

const std::vector<Vertex>& Mesh::getVertices() const {
	return this->vertices;
}