#include "TerrainGenerator.hpp"

#include <iostream>
#include <random>

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/string_cast.hpp>

TerrainGenerator::TerrainGenerator(ModelFactory& modelFactory, ResourceManager& resourceManager, std::list<Model*>& sceneries, std::list<Model*>& collidables, std::list<Model*>& bonuses):
	modelFactory(modelFactory),
	resourceManager(resourceManager),
	sceneries(sceneries),
	collidables(collidables),
	bonuses(bonuses) {

	this->reset();
}

void TerrainGenerator::reset() {
	this->distanceBeforeBranch = 0;
	this->direction = glm::ivec3(0.0f, 0.0f, -1.0f);
	this->lastBranchPosition = glm::ivec3(0, 0, 0);
	this->difficulty = 1;
	this->generatePath(glm::ivec3(0, 0, -1));
	this->generateBranch();
}

TerrainGenerator::~TerrainGenerator() {

}

void TerrainGenerator::generatePath(glm::ivec3 direction) {
	this->direction = direction;

	std::random_device rd;
	std::mt19937 mt(rd());
	std::uniform_int_distribution<int> distanceRandom(4, 13);

	this->distanceBeforeBranch = distanceRandom(mt);
	glm::ivec3 floorPosition = this->lastBranchPosition;
	glm::ivec3 ceilingPosition = this->lastBranchPosition;

	for (int i = 0; i < this->distanceBeforeBranch; i++) {
		Model* floor = this->modelFactory.loadModel(this->resourceManager.getModelPath("floor.obj"));
		Model* ceiling = this->modelFactory.loadModel(this->resourceManager.getModelPath("ceiling.obj"));
		
		if (direction.x == -1) {
			floorPosition.x -= TILE_SIZE;
		} else if (direction.x == 1) {
			floorPosition.x += TILE_SIZE;
		} else if (direction.z == -1) {
			floorPosition.z -= TILE_SIZE;
		} else if (direction.z == 1) {
			floorPosition.z += TILE_SIZE;
		}

		if (i != this->distanceBeforeBranch - 1) {
			Model* leftWall = this->modelFactory.loadModel(this->resourceManager.getModelPath("wall.obj"));
			Model* rightWall = this->modelFactory.loadModel(this->resourceManager.getModelPath("wall.obj"));

			leftWall->setPosition(floorPosition);


			rightWall->setPosition(floorPosition);
			
			if (std::fabs(direction.x) == 1) {
				leftWall->rotate(90, glm::vec3(1.0, 0.0, 1.0f));
				rightWall->rotate(90, glm::vec3(1.0, 0.0, 1.0f));

				leftWall->move(glm::vec3(0.0, TILE_SIZE / 2.0f, TILE_SIZE / 2.0f));
				rightWall->move(glm::vec3(0.0, TILE_SIZE / 2.0f, -TILE_SIZE / 2.0f));
			}
			else if (std::fabs(direction.z) == 1) {
				leftWall->rotate(90, glm::vec3(0.0, 0.0, 1.0f));
				rightWall->rotate(90, glm::vec3(0.0, 0.0, 1.0f));

				leftWall->move(glm::vec3(TILE_SIZE / 2.0f, TILE_SIZE / 2.0f, 0));
				rightWall->move(glm::vec3(-TILE_SIZE / 2.0f, TILE_SIZE / 2.0f, 0));
			}
	
			leftWall->setType(Model::Type::WALL);
			rightWall->setType(Model::Type::WALL);

			this->sceneries.push_back(leftWall);
			this->sceneries.push_back(rightWall);
		}

		floor->setPosition(floorPosition);
		this->sceneries.push_back(floor);

		ceilingPosition = floorPosition;
		ceilingPosition.y += TILE_SIZE;

		ceiling->setPosition(ceilingPosition);
		this->sceneries.push_back(ceiling);
	}

	this->generateCollidables();
	this->generateBonuses();
	this->lastBranchPosition = this->lastBranchPosition + this->direction * (this->distanceBeforeBranch * (int)TILE_SIZE);
	this->generateBranch();
}


bool TerrainGenerator::checkCollidablePosition(Model* collidable) {
	for (Model* m: this->collidables) {
		if (collidable->isCollision(m)) {
			return false;
		}
		if (std::abs(collidable->getPosition().x - m->getPosition().x) < 2.0f &&
			std::abs(collidable->getPosition().z - m->getPosition().z) < 2.0f) {
			return false;
		}
		if (this->direction.z == -1 || this->direction.z == 1) {
			if (collidable->getType() == Model::Type::NEON && m->getType() == Model::Type::NEON &&
				std::abs(collidable->getPosition().z - m->getPosition().z) < 5.0f) {
				return false;
			}
		}
		else {
			if (collidable->getType() == Model::Type::NEON && m->getType() == Model::Type::NEON &&
				std::abs(collidable->getPosition().x - m->getPosition().x) < 5.0f) {
				return false;
			}
		}
	}
	return true;
}

void TerrainGenerator::generateBranch() {

	glm::ivec3 floorPositionRight = this->lastBranchPosition;
	glm::ivec3 floorPositionLeft = this->lastBranchPosition;

	Model* floorRight = this->modelFactory.loadModel(this->resourceManager.getModelPath("floor.obj"));
	Model* floorLeft = this->modelFactory.loadModel(this->resourceManager.getModelPath("floor.obj"));

	Model* ceilingRight = this->modelFactory.loadModel(this->resourceManager.getModelPath("ceiling.obj"));
	Model* ceilingLeft = this->modelFactory.loadModel(this->resourceManager.getModelPath("ceiling.obj"));
	
	if (direction.z == -1) {
		floorPositionLeft.x -= TILE_SIZE;
		floorPositionRight.x += TILE_SIZE;
	}
	else if (direction.z == 1) {
		floorPositionLeft.x += TILE_SIZE;
		floorPositionRight.x -= TILE_SIZE;
	}
	else if (direction.x == -1) {
		floorPositionLeft.z -= TILE_SIZE;
		floorPositionRight.z += TILE_SIZE;
	}
	else {
		floorPositionLeft.z += TILE_SIZE;
		floorPositionRight.z -= TILE_SIZE;
	}
	
	floorRight->setPosition(floorPositionRight);
	floorLeft->setPosition(floorPositionLeft);

	floorPositionLeft.y += TILE_SIZE;
	floorPositionRight.y += TILE_SIZE;

	ceilingLeft->setPosition(floorPositionLeft);
	ceilingRight->setPosition(floorPositionRight);


	this->sceneries.push_back(floorRight);
	this->sceneries.push_back(floorLeft);

	this->sceneries.push_back(ceilingLeft);
	this->sceneries.push_back(ceilingRight);

	Model* bottomWall = this->modelFactory.loadModel(this->resourceManager.getModelPath("wall.obj"));
	Model* bottomLeftWall = this->modelFactory.loadModel(this->resourceManager.getModelPath("wall.obj"));
	Model* bottomRightWall = this->modelFactory.loadModel(this->resourceManager.getModelPath("wall.obj"));

	glm::ivec3 bottomWallPosition = this->lastBranchPosition;
	glm::ivec3 bottomLeftWallPosition = this->lastBranchPosition;
	glm::ivec3 bottomRightWallPosition = this->lastBranchPosition;

	bottomWallPosition.y += TILE_SIZE / 2.0f;

	if (direction.z == -1) {
		bottomWallPosition.z -= TILE_SIZE / 2.0f;
		bottomWall->rotate(90, glm::vec3(1.0f, 0.0f, 1.0f));
		bottomLeftWallPosition = bottomWallPosition;
		bottomRightWallPosition = bottomWallPosition;
		bottomLeftWallPosition.x += TILE_SIZE;
		bottomRightWallPosition.x -= TILE_SIZE;
	}
	else if (direction.z == 1) {
		bottomWallPosition.z += TILE_SIZE / 2.0f;
		bottomWall->rotate(90, glm::vec3(1.0f, 0.0f, 1.0f));

		bottomLeftWallPosition = bottomWallPosition;
		bottomRightWallPosition = bottomWallPosition;
		bottomLeftWallPosition.x += TILE_SIZE;
		bottomRightWallPosition.x -= TILE_SIZE;
	}
	else if (direction.x == -1) {
		bottomWallPosition.x -= TILE_SIZE / 2.0f;
		bottomWall->rotate(90, glm::vec3(0.0f, 0.0f, 1.0f));

		bottomLeftWallPosition = bottomWallPosition;
		bottomRightWallPosition = bottomWallPosition;
		bottomLeftWallPosition.z += TILE_SIZE;
		bottomRightWallPosition.z -= TILE_SIZE;
	}
	else {
		bottomWallPosition.x += TILE_SIZE / 2.0f;
		bottomWall->rotate(90, glm::vec3(0.0f, 0.0f, 1.0f));

		bottomLeftWallPosition = bottomWallPosition;
		bottomRightWallPosition = bottomWallPosition;
		bottomLeftWallPosition.z += TILE_SIZE;
		bottomRightWallPosition.z -= TILE_SIZE;
	}

	bottomWall->setPosition(bottomWallPosition);
	bottomLeftWall->setPosition(bottomLeftWallPosition);
	bottomRightWall->setPosition(bottomRightWallPosition);

	bottomLeftWall->setRotation(bottomWall->getRotation());
	bottomRightWall->setRotation(bottomWall->getRotation());

	this->sceneries.push_back(bottomWall);
	this->sceneries.push_back(bottomRightWall);
	this->sceneries.push_back(bottomLeftWall);
}


void TerrainGenerator::generateCollidables() {
	std::random_device rd;
	std::mt19937 mt(rd());
	std::uniform_int_distribution<int> collidableRandom(1, this->difficulty);

	std::uniform_real_distribution<float> positionRandom(3.0f, (float)this->distanceBeforeBranch - 1.0f);
	std::uniform_int_distribution<int> objectRandom(0, 2);
	std::uniform_int_distribution<int> sideRandom(0, 1);
	int nbCollidable = collidableRandom(mt);

	for (int i = 0; i < nbCollidable; i++) {

		int objectType = objectRandom(mt);
		Model* newCollidable = nullptr;
		glm::vec3 collidablePosition = this->lastBranchPosition;

		switch (objectType)
		{
		case 0:
			newCollidable = this->modelFactory.loadModel(this->resourceManager.getModelPath("ThreeTrashes.obj"));
			newCollidable->resize(glm::vec3(0.25f, 0.25f, 0.25f));
			newCollidable->setType(Model::Type::TRASHES);
			break;
		case 1:
			newCollidable = this->modelFactory.loadModel(this->resourceManager.getModelPath("AutoLaveuse.obj"));
			newCollidable->resize(glm::vec3(0.5f, 0.55f, 0.5f));
			newCollidable->setType(Model::Type::AUTOLAVEUSE);
			break;
		case 2:
			newCollidable = this->modelFactory.loadModel(this->resourceManager.getModelPath("Neon.obj"));
			newCollidable->setType(Model::Type::NEON);
			collidablePosition.y += 2.0f;
			break;
		default:
			break;
		}

		int side = sideRandom(mt);

		if (this->direction.x == -1) {
			collidablePosition.x -= positionRandom(mt) * TILE_SIZE;
			if (objectType != 2)
				collidablePosition.z += side == 0 ? -1.0f : 1.0f;
			newCollidable->rotate(90.0f, glm::vec3(0.0f, 1.0f, 0.0f));
		}
		else if (this->direction.x == 1) {
			collidablePosition.x += positionRandom(mt) * TILE_SIZE;
			if (objectType != 2)
				collidablePosition.z += side == 0 ? -1.0f : 1.0f;
			newCollidable->rotate(-90.0f, glm::vec3(0.0f, 1.0f, 0.0f));
		}
		else if (this->direction.z == -1) {
			collidablePosition.z -= positionRandom(mt) * TILE_SIZE;
			if (objectType != 2)
				collidablePosition.x += side == 0 ? -1.0f : 1.0f;
		}
		else if (this->direction.z == 1) {
			collidablePosition.z += positionRandom(mt) * TILE_SIZE;
			if (objectType != 2)
				collidablePosition.x += side == 0 ? -1.0f : 1.0f;
			newCollidable->rotate(180.0f, glm::vec3(0.0f, 1.0f, 0.0f));
		}


		newCollidable->setPosition(collidablePosition);
		newCollidable->generateAABB3DValues();

		if (this->checkCollidablePosition(newCollidable))
			this->collidables.push_back(newCollidable);
		else
			delete newCollidable;
	}

}

void TerrainGenerator::generateBonuses() {

	std::random_device rd;
	std::mt19937 mt(rd());

	std::uniform_real_distribution<float> positionRandom(3.0f, (float)this->distanceBeforeBranch);
	std::uniform_int_distribution<int> objectRandom(0, 1000);
	std::uniform_int_distribution<int> sideRandom(0, 1);

	std::uniform_int_distribution<int> nbCoinRandom(5, 10);
	if (objectRandom(mt) < 900) {
		std::uniform_real_distribution<float> coinPositionRandom(1.0f, (float)this->distanceBeforeBranch - TILE_SIZE / 2.0f);
		float position = coinPositionRandom(mt);
		glm::ivec3 coinPosition = this->lastBranchPosition;
		coinPosition.y += 1.0f;
		int side = 0;

		if (direction.x == -1) {
			coinPosition.x -= position * TILE_SIZE;
			coinPosition.z += side == 0 ? -1.0f : 1.0f;
		}
		else if (direction.x == 1) {
			coinPosition.x += position * TILE_SIZE;
			coinPosition.z += side == 0 ? -1.0f : 1.0f;
		}
		else if (direction.z == -1) {
			coinPosition.z -= position * TILE_SIZE;
			coinPosition.x += side == 0 ? -1.0f : 1.0f;
		}
		else if (direction.z == 1) {
			coinPosition.z += position * TILE_SIZE;
			coinPosition.x += side == 0 ? -1.0f : 1.0f;
		}

		bool generateCoin = true;
		for (int i = 0; i < nbCoinRandom(mt); i++) {
			Model* coin = this->modelFactory.loadModel(this->resourceManager.getModelPath("Coin.obj"));
			coin->resize(glm::vec3(0.3f, 0.3f, 0.8f));
			coin->setType(Model::Type::COIN);

			if (direction.x == -1) {
				coinPosition.x -= 3;
				if (coinPosition.x < (this->lastBranchPosition.x - (this->distanceBeforeBranch - 1.0f) * 6.0f)) {
					generateCoin = false;
				}
			}
			else if (direction.x == 1) {
				coinPosition.x += 3;
				if (coinPosition.x >(this->lastBranchPosition.x + (this->distanceBeforeBranch - 1.0f) * 6.0f)) {
					generateCoin = false;
				}
			}
			else if (direction.z == -1) {
				coinPosition.z -= 3;
				if (coinPosition.z < (this->lastBranchPosition.z - (this->distanceBeforeBranch - 1.0f) * 6.0f)) {
					generateCoin = false;
				}
			}
			else if (direction.z == 1) {
				coinPosition.z += 3;
				if (coinPosition.z > (this->lastBranchPosition.z + (this->distanceBeforeBranch - 1.0f) * 6.0f)) {
					generateCoin = false;
				}
			}


			coin->setPosition(coinPosition);
			coin->generateAABB3DValues();
			for (Model* m : this->collidables) {
				if (coin->isCollision(m)) {
					generateCoin = false;
					break;
				}
			}
			if (!generateCoin) {
				delete coin;
				break;
			}
			this->bonuses.push_back(coin);
		}
	}

	if (objectRandom(mt) < 100) {
		Model* heart = this->modelFactory.loadModel(this->resourceManager.getModelPath("Heart.obj"));
		heart->setType(Model::Type::HEART);
		int side = sideRandom(mt);

		glm::ivec3 heartPosition = this->lastBranchPosition;

		if (direction.x == -1) {
			heartPosition.x -= positionRandom(mt) * TILE_SIZE;
			heartPosition.z += side == 0 ? -1.0f : 1.0f;
		}
		else if (direction.x == 1) {
			heartPosition.x += positionRandom(mt) * TILE_SIZE;
			heartPosition.z += side == 0 ? -1.0f : 1.0f;
		}
		else if (direction.z == -1) {
			heartPosition.z -= positionRandom(mt) * TILE_SIZE;
			heartPosition.x += side == 0 ? -1.0f : 1.0f;
		}
		else if (direction.z == 1) {
			heartPosition.z += positionRandom(mt) * TILE_SIZE;
			heartPosition.x += side == 0 ? -1.0f : 1.0f;
		}

		heart->setPosition(heartPosition);
		heart->generateAABB3DValues();
		this->bonuses.push_back(heart);
	}
}

void TerrainGenerator::setDirection(glm::ivec3 direction) {
	this->direction = direction;
}

const glm::ivec3& TerrainGenerator::getLastBranchPosition() {
	return this->lastBranchPosition;
}

void TerrainGenerator::setDifficulty(int difficulty) {
	this->difficulty = difficulty;
}
