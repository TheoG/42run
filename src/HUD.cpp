#include "HUD.hpp"
#include "FileException.hpp"
#include "GenericException.hpp"

HUD::HUD(const std::string& fontPath) {
	FT_Library ft;
	if (FT_Init_FreeType(&ft))
		throw GenericException("ERROR::Freetype: Could not init FreeType Library");
	FT_Face face;
	if (FT_New_Face(ft, fontPath.c_str(), 0, &face)) {
		throw FileException(fontPath, FileException::FONT);
	}

	FT_Set_Pixel_Sizes(face, 0, 48);

	// Disable byte alignment restriction (4 bytes on the GPU)
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

	for (GLubyte c = 0; c < 128; c++) {
		if (FT_Load_Char(face, c, FT_LOAD_RENDER)) {
			throw GenericException("ERROR::Freetype: Failed to load Glyph");
		}
		GLuint texture;

		glGenTextures(1, &texture);
		glBindTexture(GL_TEXTURE_2D, texture);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RED,
			face->glyph->bitmap.width,
			face->glyph->bitmap.rows,
			0, GL_RED, GL_UNSIGNED_BYTE,
			face->glyph->bitmap.buffer);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		PrintableCharacter character = {
			texture,
			glm::ivec2(face->glyph->bitmap.width, face->glyph->bitmap.rows),
 	    	glm::ivec2(face->glyph->bitmap_left, face->glyph->bitmap_top),
			static_cast<GLuint>(face->glyph->advance.x)
		};
		this->characters.insert(std::pair<GLchar, PrintableCharacter>(c, character));
	}
	FT_Done_Face(face);
	FT_Done_FreeType(ft);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glGenVertexArrays(1, &VAO);
	glGenBuffers(1, &VBO);

	glBindVertexArray(VAO);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 6 * 4, NULL, GL_DYNAMIC_DRAW);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), 0);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
}

HUD::~HUD() {}

void HUD::addElement(const std::string& elementPath) {
	GLuint textureID = TextureLoader::textureFromFile(elementPath);
	std::string elementName = elementPath.substr(elementPath.find_last_of("/\\") + 1);
	this->textures.insert(std::pair<std::string, GLuint>(elementName, textureID));
}

void HUD::setWindowSize(int width, int height) {
	this->projection = glm::ortho(0.0f, (float)width, 0.0f, (float)height);
}

void HUD::renderElement(Shader* s, const std::string& elementName, GLfloat x, GLfloat y, GLfloat scaleX, GLfloat scaleY) {
	std::map<std::string, GLuint>::iterator it = this->textures.find(elementName);

	if (it == this->textures.end()) {
		throw GenericException("Error: This element does not exist (" + elementName + ")");
	}
	GLuint textureID = it->second;

	s->use();
	s->setMatrix("projection", this->projection);
	s->setFloat("zDepth", 0.01f);

	glActiveTexture(GL_TEXTURE0);
	glBindVertexArray(this->VAO);

	int w, h;
	int miplevel = 0;
	glBindTexture(GL_TEXTURE_2D, textureID);
	glGetTexLevelParameteriv(GL_TEXTURE_2D, miplevel, GL_TEXTURE_WIDTH, &w);
	glGetTexLevelParameteriv(GL_TEXTURE_2D, miplevel, GL_TEXTURE_HEIGHT, &h);

	w *= scaleX;
	h *= scaleY;

	GLfloat vertices[6][4] = {
		{ x,     y + h,   0.0, 0.0 },
		{ x,     y,       0.0, 1.0 },
		{ x + w, y,       1.0, 1.0 },

		{ x,     y + h,   0.0, 0.0 },
		{ x + w, y,       1.0, 1.0 },
		{ x + w, y + h,   1.0, 0.0 }
	};

	glBindBuffer(GL_ARRAY_BUFFER, this->VBO);
	glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vertices), vertices);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glDrawArrays(GL_TRIANGLES, 0, 6);
	glBindVertexArray(0);
	glBindTexture(GL_TEXTURE_2D, 0);
}

void HUD::renderText(Shader* s, const std::string& text, GLfloat x, GLfloat y, GLfloat scale, glm::vec3 color) {
	s->use();
	s->setMatrix("projection", this->projection);
	s->setVec3f("textColor", color);
	s->setFloat("zDepth", 0.001f);
	glActiveTexture(GL_TEXTURE0);
	glBindVertexArray(VAO);

	std::string::const_iterator c;
	for (c = text.begin(); c != text.end(); c++) {
		PrintableCharacter pc = this->characters[*c];

		float xpos = x + pc.bearing.x * scale;
		float ypos = y - (pc.size.y - pc.bearing.y) * scale;

		float w = pc.size.x * scale;
		float h = pc.size.y * scale;

		GLfloat vertices[6][4] = {
			{ xpos,     ypos + h,   0.0, 0.0 },            
			{ xpos,     ypos,       0.0, 1.0 },
			{ xpos + w, ypos,       1.0, 1.0 },

			{ xpos,     ypos + h,   0.0, 0.0 },
			{ xpos + w, ypos,       1.0, 1.0 },
			{ xpos + w, ypos + h,   1.0, 0.0 }           
        };

		glBindTexture(GL_TEXTURE_2D, pc.textureID);
		glBindBuffer(GL_ARRAY_BUFFER, VBO);
		glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vertices), vertices);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		
		glDrawArrays(GL_TRIANGLES, 0, 6);
	
		x += (pc.advance >> 6) * scale;
	}
	glBindVertexArray(0);
	glBindTexture(GL_TEXTURE_2D, 0);
}