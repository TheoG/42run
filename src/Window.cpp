#include "Window.hpp"

#include <exception>
#include <iostream>
#ifdef _WIN32
# include <direct.h>
#else
# include <unistd.h>
#endif

void keyboardCallback(GLFWwindow* window, int key, int scancode, int action, int mods);
void resizeCallback(GLFWwindow* window, int width, int height);
void moveWindowCallback(GLFWwindow* window, int xPos, int yPos);

Window::Window(int width, int height): width(width), height(height) {
	if (!glfwInit())
		throw std::exception();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);

#ifdef __APPLE__
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
#endif

	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	this->window = glfwCreateWindow(width, height, "42run", NULL, NULL);

	if (!this->window) {
		glfwTerminate();
		throw std::exception();
	}
	glfwMakeContextCurrent(this->window);
	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress)) {
		throw std::exception();
	}

#ifdef _WIN32
	glfwSwapInterval(0);
	int monitorWidth, monitorHeight;

	const GLFWvidmode * mode = glfwGetVideoMode(glfwGetPrimaryMonitor());

	monitorWidth = mode->width;
	monitorHeight = mode->height;

	glfwSetWindowPos(window, monitorWidth / 2 - width / 2, monitorHeight / 2 - height / 2);
#endif

	glfwSetWindowUserPointer(this->window, this);
	glfwSetKeyCallback(this->window, keyboardCallback);
	glfwSetWindowSizeCallback(this->window, resizeCallback);
	glfwSetWindowPosCallback(this->window, moveWindowCallback);

	glfwSetWindowSizeLimits(this->window, DEFAULT_WIDTH / 1.5f, DEFAULT_HEIGHT / 1.5f, GLFW_DONT_CARE, GLFW_DONT_CARE);
	char buffer[2048];
	std::string path = getcwd(buffer, sizeof(buffer));
	this->resourceManager.setResourcePath(path);
	this->game = new Game(this->modelFactory, this->resourceManager);
	this->game->setWindow(this);
	this->game->resize(width, height);
	this->s = new Shader(this->resourceManager.getShaderPath("base.vert"), this->resourceManager.getShaderPath("base.frag"));
	this->printerShader = new Shader(this->resourceManager.getShaderPath("glyph.vert"), this->resourceManager.getShaderPath("glyph.frag"));
	this->textureShader = new Shader(this->resourceManager.getShaderPath("glyph.vert"), this->resourceManager.getShaderPath("hud.frag"));
	this->deltaTime = 0.0f;
}


Window::~Window() {
	delete this->s;
	delete this->printerShader;
	delete this->textureShader;
	delete this->game;
	glfwDestroyWindow(this->window);
	glfwTerminate();
}

void Window::display() {

	float lastFrame = 0.0f;
	float waitingLastFrame = 0.0f;
	float cumulatedDeltaTime = 0.0f;

	this->s->use();
	s->setMatrix("projectionMatrix", this->game->getCamera().getProjectionMatrix());
	glEnable(GL_DEPTH_TEST);

	bool end = false;
	while(!glfwWindowShouldClose(this->window) && !end)	{
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT);
		
		switch (this->game->getState()) {
		case Game::State::MAIN_MENU:
			this->game->renderMainMenu(this->s, this->printerShader, this->textureShader, deltaTime);
			lastFrame = glfwGetTime();
			waitingLastFrame = lastFrame;
			break;
		case Game::State::RETRY:
			this->game->renderRetryMenu(this->s, this->printerShader, this->textureShader, deltaTime);
			lastFrame = glfwGetTime();
			waitingLastFrame = lastFrame;
			break;
		case Game::State::END:
			end = true;
			break;
		case Game::State::PAUSE:
			lastFrame = glfwGetTime();
			waitingLastFrame = lastFrame;
		case Game::State::PLAY:
			float currentFrame = glfwGetTime();
			this->deltaTime = currentFrame - lastFrame;	
			lastFrame = currentFrame;

			cumulatedDeltaTime = 0.0f;
			float waitingDeltaTime = currentFrame - waitingLastFrame;
			// Limit the render - 60 FPS
			if (!this->game->isPaused()) {
				while (cumulatedDeltaTime < 0.016f) {
					float currentFrame = glfwGetTime();
					waitingDeltaTime = currentFrame - waitingLastFrame;
					this->game->update(waitingDeltaTime);
					waitingLastFrame = currentFrame;
					cumulatedDeltaTime += waitingDeltaTime;
				}
				if ((int)(1000.0 / this->deltaTime / 1000) < 15) {
					std::cout << "FPS too low (" << (int)(1000.0 / this->deltaTime / 1000) << "). You may not be able to play: " << this->deltaTime << std::endl;
				}
				this->processInput(deltaTime);
				this->updateWindowTitle();
				s->setMatrix("viewMatrix", this->game->getCamera().getViewMatrix());
			}

			this->game->render(this->s, this->printerShader, this->textureShader, cumulatedDeltaTime);
			break;
		}

		glfwSwapBuffers(this->window);
		glfwPollEvents();
	}
}


void Window::resize(int width, int height) {
	this->width = width;
	this->height = height;
	this->s->setMatrix("projectionMatrix", this->game->getCamera().getProjectionMatrix());
	this->game->resize(width, height);
}

void Window::updateWindowTitle() {
	char		title[128];
	static double	lastTime = 0.0;

	lastTime += this->deltaTime;

	if (lastTime >= 1.0) {
		sprintf(title, "42run - %.3f ms (%d FPS)", this->deltaTime, (int)(1000.0 / this->deltaTime / 1000));
		glfwSetWindowTitle(this->window, title);
		lastTime = 0.0f;
	}
}

void Window::processInput(float deltaTime) {
	if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS) {
		glfwSetWindowShouldClose(this->window, true);
	}
	if (!this->game->isPaused()) {
		this->game->processInput(this->window, deltaTime);
	}
}

void keyboardCallback(GLFWwindow* window, int key, int scancode, int action, int mods) {
	(void)scancode;
	(void)mods;
	Window* w = (Window*)glfwGetWindowUserPointer(window);

	if (w->getGame()->getState() == Game::State::MAIN_MENU) {
		if (key == GLFW_KEY_DOWN && action == GLFW_PRESS) {
			w->getGame()->increaseMainMenuPosition();
		}
		if (key == GLFW_KEY_UP && action == GLFW_PRESS) {
			w->getGame()->decreaseMainMenuPosition();
		}
		if (key == GLFW_KEY_ENTER && action == GLFW_PRESS) {
			if (w->getGame()->getMainMenuPosition() == 0) {
				w->getGame()->setState(Game::State::PLAY);
			}
			else if (w->getGame()->getMainMenuPosition() == 1) {
				w->getGame()->setState(Game::State::END);
			}
		}
		return;
	}
	else if (w->getGame()->getState() == Game::State::RETRY) {
		if (key == GLFW_KEY_RIGHT && action == GLFW_PRESS) {
			w->getGame()->increaseRetryMenuPosition();
		}
		if (key == GLFW_KEY_LEFT && action == GLFW_PRESS) {
			w->getGame()->decreaseRetryMenuPosition();
		}
		if (key == GLFW_KEY_ENTER && action == GLFW_PRESS) {
			if (w->getGame()->getRetryMenuPosition() == 0) {
				w->getGame()->reset();
				w->getGame()->setState(Game::State::PLAY);
			}
			else if (w->getGame()->getRetryMenuPosition() == 1) {
				w->getGame()->setState(Game::State::END);
			}
		}
		return;
	}
	if (key == GLFW_KEY_R && action == GLFW_PRESS && w->getGame()->getState() == Game::State::PLAY) {
		w->getGame()->reset();
	}
	if (key == GLFW_KEY_P && action == GLFW_PRESS) {
		w->getGame()->setPause(!w->getGame()->isPaused());
	}
	if (w->getGame()->isPaused())
		return;
	if ((key == GLFW_KEY_DOWN || key == GLFW_KEY_S) && action == GLFW_PRESS) {
		w->getGame()->getPlayer()->crouch();
	}
	if (key == GLFW_KEY_LEFT && action == GLFW_PRESS) {
		w->getGame()->rotate(-90);
	}
	if (key == GLFW_KEY_RIGHT && action == GLFW_PRESS) {
		w->getGame()->rotate(90);
	}
}

void resizeCallback(GLFWwindow* window, int width, int height) {
	Window* w = (Window*)glfwGetWindowUserPointer(window);
	w->resize(width, height);
	glViewport(0, 0, width, height);
}

void moveWindowCallback(GLFWwindow* window, int xPos, int yPos) {
	Window* w = (Window*)glfwGetWindowUserPointer(window);
	if (w->getGame()->getState() == Game::State::PLAY) {
		w->getGame()->setPause(true);
	}
}

GLFWwindow* Window::getWindow() const {
	return this->window;
}

Game* Window::getGame() const {
	return this->game;
}