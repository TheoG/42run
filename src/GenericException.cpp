#include "GenericException.hpp"

GenericException::GenericException(const std::string& message) {
	this->message = message;
}

const char* GenericException::what() const throw() {
	return message.c_str();
}