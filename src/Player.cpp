#include "Player.hpp"

Player::Player(std::vector<Mesh> meshes): Model(meshes) {
	this->reset();
}

Player::Player(Model& other): Model(other) {
	this->reset();
}

Player::~Player() {}

void Player::reset() {
	this->score		= 0.0f;
	this->nbCoins	= 0;
	this->distance	= 0.0f;
	this->life		= 3;
	this->crouchTime = -1.0;
	this->direction	= Player::Direction::NORTH;
	this->position	= glm::vec3(0.0f, 0.0f, 0.0f);
	this->rotation	= glm::vec3(0.0f, 0.0f, 0.0f);
	this->size		= glm::vec3(1.0f, 1.0f, 0.0f);
	this->damageCooldown = 0.0f;

	this->updateModelMatrix();

	this->type = Model::Type::PLAYER;
}

void Player::processInput(GLFWwindow* winPtr, float deltaTime) {
	// Going Left
	if (glfwGetKey(winPtr, GLFW_KEY_A) == GLFW_PRESS) {
		switch (this->direction) {
			case Direction::NORTH:
				if (this->position.x > this->boundaries.x - 1.0) {
					this->move(this->rotationMatrix * glm::vec4(-10.0 * deltaTime, 0.0, 0.0, 1.0));
				}
			break;
			case Direction::SOUTH:
				if (this->position.x < this->boundaries.x + 1.0) {
					this->move(this->rotationMatrix * glm::vec4(-10.0 * deltaTime, 0.0, 0.0, 1.0));
				}
			break;
			case Direction::WEST:
				if (this->position.z < this->boundaries.z + 1.0) {
					this->move(this->rotationMatrix * glm::vec4(-10.0 * deltaTime, 0.0, 0.0, 1.0));
				}
			break;
			case Direction::EAST:
				if (this->position.z > this->boundaries.z - 1.0) {
					this->move(this->rotationMatrix * glm::vec4(-10.0 * deltaTime, 0.0, 0.0, 1.0));
				}
			break;
		}
		
	}
	// Going right
	if (glfwGetKey(winPtr, GLFW_KEY_D) == GLFW_PRESS) {
		switch (this->direction) {
			case Direction::NORTH:
				if (this->position.x < this->boundaries.x + 1.0) {
					this->move(this->rotationMatrix * glm::vec4(10.0 * deltaTime, 0.0, 0.0, 1.0));
				}
			break;
			case Direction::SOUTH:
				if (this->position.x > this->boundaries.x - 1.0) {
					this->move(this->rotationMatrix * glm::vec4(10.0 * deltaTime, 0.0, 0.0, 1.0));
				}
			break;
			case Direction::WEST:
				if (this->position.z > this->boundaries.z - 1.0) {
					this->move(this->rotationMatrix * glm::vec4(10.0 * deltaTime, 0.0, 0.0, 1.0));
				}
			break;
			case Direction::EAST:
				if (this->position.z < this->boundaries.z + 1.0) {
					this->move(this->rotationMatrix * glm::vec4(10.0 * deltaTime, 0.0, 0.0, 1.0));
				}
			break;
		}	
	}
	// Jump
	if (glfwGetKey(winPtr, GLFW_KEY_UP) == GLFW_PRESS || glfwGetKey(winPtr, GLFW_KEY_W) == GLFW_PRESS || glfwGetKey(winPtr, GLFW_KEY_SPACE) == GLFW_PRESS) {
		if (this->getPosition().y <= 0.01) {
			this->setVelocity(glm::vec3(0.0f, 10.0f, 0.0f));
		}
	}
}

void Player::crouch() {
	if (this->crouchTime > 0.0f)
		return;
	this->crouchTime = 0.0f;
	this->setSize(glm::vec3(1.0, 0.5, 1.0));
	this->generateAABB3DValues();
}

void Player::straighten() {
	this->setSize(glm::vec3(1.0, 1.0, 1.0));
	this->generateAABB3DValues();
}

bool Player::isCrouched() {
	return this->crouchTime >= 0.0f;
}

float Player::getCrouchTime() const {
	return this->crouchTime;
}

void Player::increaseCrouchTime(float deltaTime) {
	this->crouchTime += deltaTime;
}

void Player::setCrouchTime(float time) {
	this->crouchTime = time;
}

float Player::getScore() const {
	return this->score;
}

float Player::getDistance() const {
	return this->distance;
}

int Player::getNbCoins() const {
	return this->nbCoins;
}

short Player::getLife() const {
	return this->life;
}

Player::Direction Player::getDirection() const {
	return this->direction;
}

void Player::rotate(float angle, glm::vec3 axis) {
	Model::rotate(angle, axis);
	this->direction = (Player::Direction)((int)this->direction + (angle > 0 ? 1 : -1));
	if (this->direction > Player::Direction::EAST)
		this->direction = Player::Direction::NORTH;
	if ((int)this->direction == -1)
		this->direction = Player::Direction::EAST;
}

void Player::setBoundaries(glm::vec3 boundaries) {
	this->boundaries = boundaries;
}

void Player::increaseScore(float score) {
	this->score += score;
}

void Player::increaseDistance(float distance) {
	this->distance += distance;
}

void Player::increaseNbCoin() {
	this->nbCoins++;
}

void Player::setScore(float score) {
	this->score = score;
}

void Player::takeDamage(Damage damageType, short damage) {
	if (this->damageCooldown >= 0.0f)
		return;
	this->life -= damage;
	if (this->life < 0)
		this->life = 0;
	this->damageType = damageType;
	this->damageCooldown = Player::defaultDamageCooldown;
}

void Player::heal(short hp) {
	this->life += hp;
	if (this->life > 3) {
		this->score += 500;
		this->life = 3;
	}
}

void Player::decreaseDamageCooldown(float delta) {
	this->damageCooldown -= delta;
}

const glm::vec3& Player::getBoundaries() const {
	return this->boundaries;
}

float Player::getDamageCooldown() const {
	return this->damageCooldown;
}

bool Player::hasToBlink() {
	if (this->damageCooldown >= 0.0f) {
		return (int)(this->damageCooldown * 1000) % 400 < 200;
	}
	return false;
}

Player::Damage Player::getDamageType() const {
	return this->damageType;
}

const float Player::defaultDamageCooldown = 2.0f;
const float Player::crouchDuration = 0.7f;
const int	Player::nbFrameTextureAnimation = 4;