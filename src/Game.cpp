#include "Game.hpp"

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/string_cast.hpp>

#include <random>

Game::Game(ModelFactory& modelFactory, ResourceManager& resourceManager): modelFactory(modelFactory), resourceManager(resourceManager) {

	this->setupPlayer();
	this->camera.setLookAt(glm::vec3(0.0, 2.5, 0.0));
	this->remainingRotation = 0.0f;
	this->mainMenuPosition	= 0;
	this->retryMenuPosition	= 0;
	this->paused			= false;
	this->state				= Game::State::MAIN_MENU;
	this->boundaries		= this->player->getPosition();
	this->hud				= new HUD(this->resourceManager.getFontPath("arcade.ttf"));
	this->hud->setWindowSize(DEFAULT_WIDTH, DEFAULT_HEIGHT);

	this->hud->addElement(this->resourceManager.getModelPath("MainMenuPlay.png"));
	this->hud->addElement(this->resourceManager.getModelPath("MainMenuExit.png"));
	this->hud->addElement(this->resourceManager.getModelPath("RetryMenuRetry.png"));
	this->hud->addElement(this->resourceManager.getModelPath("RetryMenuExit.png"));
	this->hud->addElement(this->resourceManager.getModelPath("heart.png"));
	this->hud->addElement(this->resourceManager.getModelPath("emptyheart.png"));

	this->windowSize	= glm::ivec2(DEFAULT_WIDTH, DEFAULT_HEIGHT);	
	this->generator		= new TerrainGenerator(modelFactory, resourceManager, sceneries, collidables, bonuses);

	// Avoid freeze during the game generation
	this->modelFactory.preloadModel(this->resourceManager.getModelPath("ThreeTrashes.obj"));
	this->modelFactory.preloadModel(this->resourceManager.getModelPath("AutoLaveuse.obj"));
	this->modelFactory.preloadModel(this->resourceManager.getModelPath("Neon.obj"));
	this->modelFactory.preloadModel(this->resourceManager.getModelPath("Coin.obj"));
	this->modelFactory.preloadModel(this->resourceManager.getModelPath("Heart.obj"));
}

void Game::reset() {
	for (Model* m : this->collidables) {
		delete m;
	}
	for (Model* m : this->sceneries) {
		delete m;
	}
	for (Model* m : this->bonuses) {
		delete m;
	}
	this->sceneries.clear();
	this->bonuses.clear();
	this->collidables.clear();

	this->remainingRotation = 0.0f;
	this->paused			= false;

	this->player->reset();
	this->boundaries = this->player->getPosition();
	this->generator->reset();
	this->camera.reset();
}

Game::~Game() {
	delete player;

	for (Model* m: this->collidables) {
		delete m;
	}
	for (Model* m: this->sceneries) {
		delete m;
	}
	for (Model* m : this->bonuses) {
		delete m;
	}
	this->collidables.clear();
	this->sceneries.clear();
	this->bonuses.clear();

	delete generator;
	delete hud;
}

void Game::setupPlayer() {

	Model* modelPlayer = this->modelFactory.loadModel(this->resourceManager.getModelPath("Player.obj"));
	this->player = new Player(*modelPlayer);
	delete modelPlayer;

	std::vector<Mesh>& meshes = this->player->getMeshes();

	// Iterate through the number of texture for the player animation
	for (int i = 0; i < Player::nbFrameTextureAnimation; i++) {	
		Texture playerTexture = this->modelFactory.loadTexture("SinglePlayer" + std::to_string(i) + ".png");
		for (Mesh& m: meshes) {
			m.addTexture(playerTexture);
		}
	}
	this->player->generateAABB3DValues();
}

void Game::processInput(GLFWwindow* winPtr, float deltaTime) {
	this->player->processInput(winPtr, deltaTime);
}

void Game::update(float deltaTime) {
	this->updatePlayer(deltaTime);
	this->updateEntities(deltaTime);
	this->updateBonuses(deltaTime);
	this->updateCamera(deltaTime);
}

void Game::render(Shader* shader, Shader* printerShader, Shader* textureShader, float deltaTime) {
	this->renderEntities(shader, deltaTime);
	this->renderBonuses(shader, deltaTime);
	this->renderPlayer(shader, deltaTime);
	this->renderHUD(printerShader, textureShader);
}

void Game::updatePlayer(float deltaTime) {
	glm::vec3 playerPosition = this->player->getPosition();
	glm::vec3 playerVelocity = this->player->getVelocity();

	float movingForwardSpeed = 12.5f * deltaTime;

	// Jump velocity
	playerPosition.y += playerVelocity.y * deltaTime;
	
	// Normalized player direction
	glm::vec3 playerDirection = this->player->getRotationMatrix() * glm::vec4(0.0f, 0.0f, -1.0f, 1.0f);

	// Increase the player position depending on his direction
	playerPosition += playerDirection * movingForwardSpeed;
	
	// Update the player velocity by the gravity
	playerVelocity += this->player->gravity * deltaTime;

	// Increase the player score by the same amount of his speed
	this->player->increaseScore(movingForwardSpeed);
	this->player->increaseDistance(movingForwardSpeed);

	// If he's falling, don't let him go <3
	if (playerPosition.y < 0.0f) {
		playerPosition.y = 0.0f;
		playerVelocity.y = 0.0f;
	}

	this->player->setPosition(playerPosition);
	this->player->setVelocity(playerVelocity);

	this->player->decreaseDamageCooldown(deltaTime);

	this->player->setBoundaries(this->boundaries);

	switch (this->player->getDirection()) {
	case Player::Direction::NORTH:
		if (this->player->getPosition().z < (this->generator->getLastBranchPosition().z - TILE_SIZE / 2.0f)) {
			this->state = Game::State::RETRY;
		}
		break;
	case Player::Direction::SOUTH:
		if (this->player->getPosition().z > (this->generator->getLastBranchPosition().z + TILE_SIZE / 2.0f)) {
			this->state = Game::State::RETRY;
		}
		break;
	case Player::Direction::EAST:
		if (this->player->getPosition().x > (this->generator->getLastBranchPosition().x + TILE_SIZE / 2.0f)) {
			this->state = Game::State::RETRY;
		}
		break;
	case Player::Direction::WEST:
		if (this->player->getPosition().x < (this->generator->getLastBranchPosition().x - TILE_SIZE / 2.0f)) {
			this->state = Game::State::RETRY;
		}
		break;
	}

	if ((int)this->player->getDistance() % 200 == 0) {
		this->generator->setDifficulty((int)this->player->getDistance() / 200 + 1);
	}

	if (this->player->isCrouched()) {
		this->player->increaseCrouchTime(deltaTime);
		if (this->player->getCrouchTime() > Player::crouchDuration) {
			this->player->straighten();
		}
		if (this->player->getCrouchTime() > Player::crouchDuration + 0.3f) {
			this->player->setCrouchTime(-1.0f);
		}
	}
}

void Game::updateEntities(float deltaTime) {
	std::list<Model*> collidableToRemove;
	std::list<Model*> sceneriesToRemove;

	(void)deltaTime;
	for (Model* m: this->collidables) {
		if (this->player->isCollision(m)) {
			collidableToRemove.push_back(m);
			if (this->player->getDamageCooldown() <= 0.0f) {
				if (this->player->getLife() == 0) {
					this->state = Game::State::RETRY;
				}
				this->player->takeDamage(Player::Damage::COLLIDABLE, 1);
			}
		}
		glm::vec3 playerDirection = glm::vec3(this->player->getRotationMatrix() * glm::vec4(0.0f, 0.0f, -1.0f, 1.0f));
		glm::vec3 toTarget = m->getPosition() - this->player->getPosition();
		if (glm::dot(toTarget, playerDirection) <= -9.0f) {
			collidableToRemove.push_back(m);
		}
	}

	for (Model* m: this->sceneries) {
		glm::vec3 playerDirection = glm::vec3(this->player->getRotationMatrix() * glm::vec4(0.0f, 0.0f, -1.0f, 1.0f));
		glm::vec3 toTarget = m->getPosition() - this->player->getPosition();

		if (this->remainingRotation && std::abs(this->remainingRotation) < 70.0f && glm::dot(toTarget, playerDirection) <= -1.0f) {
			if (m->getType() == Model::Type::WALL)
				sceneriesToRemove.push_back(m);
		}

		if (glm::dot(toTarget, playerDirection) <= -9.0f) {
			sceneriesToRemove.push_back(m);
		}
	}

	for (Model* m: collidableToRemove) {
		this->collidables.remove(m);
		delete m;
	}
	for (Model* m: sceneriesToRemove) {
		this->sceneries.remove(m);
		delete m;
	}
	
}

void Game::updateBonuses(float deltaTime) {
	std::list<Model*> bonusesToRemove;
	glm::vec3 playerDirection = glm::vec3(this->player->getRotationMatrix() * glm::vec4(0.0f, 0.0f, -1.0f, 1.0f));
	glm::vec3 playerPosition = this->player->getPosition();
	for (Model* m : this->bonuses) {
		if (m->getType()  == Model::Type::COIN) {
			if (this->player->isCollision(m)) {
				bonusesToRemove.push_back(m);
				this->player->increaseNbCoin();
				this->player->increaseScore(10.0f);
			}
		}
		else if (m->getType() == Model::Type::HEART) {
			if (this->player->isCollision(m)) {
				bonusesToRemove.push_back(m);
				this->player->heal(1);
			}
		}
		glm::vec3 toTarget = m->getPosition() - playerPosition;
		if (glm::dot(toTarget, playerDirection) <= -9.0f) {
			bonusesToRemove.push_back(m);
		}	
		m->rotate(200.0f * deltaTime, glm::vec3(0.0f, 1.0f, 0.0f));
		m->generateAABB3DValues();
	}

	for (Model* m : bonusesToRemove) {
		this->bonuses.remove(m);
		delete m;
	}
}

void Game::updateCamera(float deltaTime) {

	glm::vec3 playerPosition = player->getPosition();
	glm::vec3 cameraPosition;

	Player::Direction playerDirection = this->player->getDirection();

	if (this->remainingRotation != 0) {
		float result = deltaTime * 250.0f;

		if (this->remainingRotation > 0) {
			this->remainingRotation -= result;
			if (this->remainingRotation < 0.0f)
				this->remainingRotation = 0.0f;
		}
		else {
			this->remainingRotation += result;
			if (this->remainingRotation > 0.0f)
				this->remainingRotation = 0.0f;
		}
	}
	playerPosition.y = 0.0f;
	float radianAngle = glm::radians((float)this->remainingRotation);
	if (playerDirection == Player::Direction::NORTH) {
		cameraPosition = playerPosition + glm::vec3(10.0f * std::sin(radianAngle), CAMERA_POS_Y, CAMERA_POS_Z * std::cos(radianAngle));
		if (!this->remainingRotation)
			cameraPosition.x = this->boundaries.x;
		playerPosition.x = this->boundaries.x;
	}
	else if (playerDirection == Player::Direction::WEST) {
		cameraPosition = playerPosition + glm::vec3(CAMERA_POS_Z * std::cos(-radianAngle), CAMERA_POS_Y, 10.0f * std::sin(-radianAngle));
		if (!this->remainingRotation)
			cameraPosition.z = this->boundaries.z;
		playerPosition.z = this->boundaries.z;
	}
	else if (playerDirection == Player::Direction::SOUTH) {
		cameraPosition = playerPosition + glm::vec3(-10.0f * std::sin(radianAngle), CAMERA_POS_Y, -CAMERA_POS_Z * std::cos(radianAngle));
		if (!this->remainingRotation)
			cameraPosition.x = this->boundaries.x;
		playerPosition.x = this->boundaries.x;
	}
	else if (playerDirection == Player::Direction::EAST) {
		cameraPosition = playerPosition + glm::vec3(-CAMERA_POS_Z * std::cos(-radianAngle), CAMERA_POS_Y, -10.0f * std::sin(-radianAngle));
		if (!this->remainingRotation)
			cameraPosition.z = this->boundaries.z;
		playerPosition.z = this->boundaries.z;
	}
	playerPosition.y += 1.5f;

	if (this->player->getDamageCooldown() > Player::defaultDamageCooldown - 0.15f && this->player->getDamageType() == Player::Damage::WALL) {
		std::random_device rd;
		std::mt19937 mt(rd());
		std::uniform_real_distribution<float> distanceRandom(0.0f, 0.2f);
		playerPosition.x += distanceRandom(mt);
		playerPosition.z += distanceRandom(mt);
	}

	this->camera.setLookAt(playerPosition);
	this->camera.setPosition(cameraPosition);
}

void Game::renderPlayer(Shader* shader, float deltaTime) {
	shader->setMatrix("modelMatrix", player->getModelMatrix());

	if (this->player->hasToBlink()) {
		shader->setBool("isHurt", true);
	}

	this->player->draw(shader, deltaTime);
	shader->setBool("isHurt", false);
}

void Game::renderEntities(Shader *shader, float deltaTime) {
	for (Model* m: this->collidables) {
		shader->setMatrix("modelMatrix", m->getModelMatrix());
		m->draw(shader, deltaTime);
	}

	for (Model* m: this->sceneries) {
		shader->setMatrix("modelMatrix", m->getModelMatrix());
		m->draw(shader, deltaTime);
	}
}

void Game::renderBonuses(Shader* shader, float deltaTime) {
	for (Model* m : this->bonuses) {
		shader->setMatrix("modelMatrix", m->getModelMatrix());
		m->draw(shader, deltaTime);
	}
}

void Game::renderHUD(Shader* textShader, Shader* textureShader) {
	hud->renderText(textShader, "Score   " + std::to_string((int)this->player->getScore()), 25.0f, 25.0f, 1.0f, glm::vec3(1.0f, 0.239f, 0.0f));
	hud->renderText(textShader, "Coins   " + std::to_string(this->player->getNbCoins()), 25.0f, 75.0f, 1.0f, glm::vec3(1.0f, 0.239f, 0.0f));

	if (this->paused) {
		hud->renderText(textShader, "PAUSE", windowSize.x / 2 - 150, windowSize.y / 2, 2.0f, glm::vec3(1.0f, 0.1f, 0.1f));
	}
	this->hud->renderElement(textureShader, this->player->getLife() > 2 ? "heart.png" : "emptyheart.png", this->windowSize.x - 400.0f, this->windowSize.y - 125.0f, 0.25f, 0.25f);
	this->hud->renderElement(textureShader, this->player->getLife() > 1 ? "heart.png" : "emptyheart.png", this->windowSize.x - 275.0f, this->windowSize.y - 125.0f, 0.25f, 0.25f);
	this->hud->renderElement(textureShader, this->player->getLife() > 0 ? "heart.png" : "emptyheart.png", this->windowSize.x - 150.0f, this->windowSize.y - 125.0f, 0.25f, 0.25f);
}

void Game::resize(int width, int height) {
	this->hud->setWindowSize(width, height);
	this->camera.setProjectionSettings(45.0f, width, height, 0.1f, 100.0f);
	this->windowSize = glm::ivec2(width, height);
}

void Game::rotate(float angle) {
	if (this->remainingRotation > 0.0f)
		return;

	Player::Direction direction = this->player->getDirection();

	if (direction == Player::Direction::NORTH || direction == Player::Direction::SOUTH) {
		if (std::fabs(std::fabs(this->player->getPosition().z) - std::fabs(this->generator->getLastBranchPosition().z)) > TILE_SIZE / 2.0f) {
			if (this->player->getLife() == 0) {
				this->state = Game::State::RETRY;
			}
			this->player->takeDamage(Player::Damage::WALL, 1);
			return;
		}
	}
	else if (direction == Player::Direction::EAST || direction == Player::Direction::WEST) {
		if (std::fabs(std::fabs(this->player->getPosition().x) - std::fabs(this->generator->getLastBranchPosition().x)) > TILE_SIZE / 2.0f) {
			if (this->player->getLife() == 0) {
				this->state = Game::State::RETRY;
			}
			this->player->takeDamage(Player::Damage::WALL, 1);
			return;
		}
	}

	this->remainingRotation = angle;

	glm::vec3 oldPosition = this->generator->getLastBranchPosition();
	glm::vec3 oldBoundaries = this->boundaries;
	this->boundaries = oldPosition;
	this->player->rotate(-angle, glm::vec3(0.0f, 1.0f, 0.0f));
	float shift = 0.0f;
	switch (this->player->getDirection()) {
	case Player::Direction::NORTH:
		shift = oldBoundaries.z - this->player->getPosition().z;
		oldPosition.x -= angle > 0 ? -shift : shift;
		this->generator->generatePath(glm::ivec3(0, 0, -1));
		break;
	case Player::Direction::SOUTH:
		shift = oldBoundaries.z - this->player->getPosition().z;
		oldPosition.x -= angle > 0 ? -shift : shift;
		this->generator->generatePath(glm::ivec3(0, 0, 1));
		break;
	case Player::Direction::EAST:
		shift = oldBoundaries.x - this->player->getPosition().x;
		oldPosition.z += angle > 0 ? -shift : shift;
		this->generator->generatePath(glm::ivec3(1, 0, 0));
		break;
	case Player::Direction::WEST:
		shift = oldBoundaries.x - this->player->getPosition().x;
		oldPosition.z += angle > 0 ? -shift : shift;
		this->generator->generatePath(glm::ivec3(-1, 0, 0));
		break;
	}
	oldPosition.y = this->player->getPosition().y;
	this->player->setPosition(oldPosition);
	this->player->generateAABB3DValues();
}

void Game::renderMainMenu(Shader* shader, Shader* printerShader, Shader* textureShader, float deltaTime) {	
	switch (this->mainMenuPosition) {
	case 0:
		this->hud->renderElement(textureShader, "MainMenuPlay.png", 0.0f, 0.0f, this->windowSize.x / 2560.0f, this->windowSize.y / 1440.0f);
		break;
	case 1:
		this->hud->renderElement(textureShader, "MainMenuExit.png", 0.0f, 0.0f, this->windowSize.x / 2560.0f, this->windowSize.y / 1440.0f);
		break;
	}
}

void Game::renderRetryMenu(Shader* shader, Shader* printerShader, Shader* textureShader, float deltaTime) {
	switch (this->retryMenuPosition) {
	case 0:
		this->hud->renderElement(textureShader, "RetryMenuRetry.png", 0.0f, 0.0f, this->windowSize.x / 2560.0f, this->windowSize.y / 1440.0f);
		break;
	case 1:
		this->hud->renderElement(textureShader, "RetryMenuExit.png", 0.0f, 0.0f, this->windowSize.x / 2560.0f, this->windowSize.y / 1440.0f);
		break;
	}
	std::string score = std::to_string((int)this->player->getScore());
	size_t n = std::count(score.begin(), score.end(), '1');
	int xShift = ((score.length() - n) * 24.0f) + (n * 24.0f);
	this->hud->renderText(printerShader, score, windowSize.x / 2.0f - xShift, windowSize.y / 2 - 50.0f, 2.0f, glm::vec3(0.0f, 0.0f, 0.0f));
}

void Game::increaseMainMenuPosition() {
	++this->mainMenuPosition;
	if (this->mainMenuPosition > 1) {
		this->mainMenuPosition = 0;
	}
}

void Game::decreaseMainMenuPosition() {
	--this->mainMenuPosition;
	if (this->mainMenuPosition < 0) {
		this->mainMenuPosition = 1;
	}
}

short Game::getMainMenuPosition() const {
	return this->mainMenuPosition;
}

void Game::increaseRetryMenuPosition() {
	++this->retryMenuPosition;
	if (this->retryMenuPosition > 1) {
		this->retryMenuPosition = 0;
	}
}

void Game::decreaseRetryMenuPosition() {
	--this->retryMenuPosition;
	if (this->retryMenuPosition < 0) {
		this->retryMenuPosition = 1;
	}
}

short Game::getRetryMenuPosition() const {
	return this->retryMenuPosition;
}

void Game::setState(Game::State state) {
	this->state = state;
}

void Game::setWindow(Window* window) {
	this->camera.setWindow(window);
}

Player* Game::getPlayer() const {
	return this->player;
}

Camera& Game::getCamera() {
	return this->camera;
}

bool Game::isPaused() const {
	return this->paused;
}

void Game::setPause(bool paused) {
	this->paused = paused;
	this->state = this->paused ? Game::State::PAUSE : Game::State::PLAY;
}

Game::State Game::getState() const {
	return this->state;
}