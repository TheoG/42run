#define STB_IMAGE_IMPLEMENTATION
#include "Model.hpp"

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/string_cast.hpp>

Model::Model(std::vector<Mesh> meshes) {
	this->meshes = meshes;

	this->position	= glm::vec3(0.0f, 0.0f, 0.0f);
	this->rotation	= glm::vec3(0.0f, 0.0f, 0.0f);
	this->size		= glm::vec3(1.0f, 1.0f, 1.0f);

	this->velocity	= glm::vec3(0.0f, 0.0f, 0.0f);

	this->aabbPosition.x = INFINITY;
	this->aabbPosition.y = INFINITY;
	this->aabbPosition.z = INFINITY;

	this->aabbSize.x = -INFINITY;
	this->aabbSize.y = -INFINITY;
	this->aabbSize.z = -INFINITY;

	this->type = Model::Type::DEFAULT;

	this->updateModelMatrix();
}

Model::Model(Model& other) {
	this->meshes = other.meshes;

	this->velocity = other.velocity;

	this->position = other.position;
	this->rotation = other.rotation;
	this->size = other.size;

	this->modelMatrix = other.modelMatrix;

	this->aabbPosition = other.aabbPosition;
	this->aabbSize = other.aabbSize;
	this->worldToLocalPosition = other.worldToLocalPosition;
}

Model::~Model() {}

void Model::draw(Shader* shader, float deltaTime) {
	for (Mesh& m : this->meshes) {
		m.draw(shader, deltaTime);
	}
}

bool Model::isCollision(Model* other) {
	return (!((other->aabbPosition.x >= this->aabbPosition.x + this->aabbSize.x) ||
		(other->aabbPosition.x + other->aabbSize.x <= this->aabbPosition.x) ||

		(other->aabbPosition.y >= this->aabbPosition.y + this->aabbSize.y) ||
		(other->aabbPosition.y + other->aabbSize.y <= this->aabbPosition.y) ||

		(other->aabbPosition.z >= this->aabbPosition.z + this->aabbSize.z) ||
		(other->aabbPosition.z + other->aabbSize.z <= this->aabbPosition.z)));
}

const glm::mat4& Model::getModelMatrix() const {
	return this->modelMatrix;
}

const glm::vec3& Model::getVelocity() const {
	return this->velocity;
}

const glm::vec3& Model::getPosition() const {
	return this->position;
}

const glm::vec3& Model::getRotation() const {
	return this->rotation;
}

const glm::mat4& Model::getRotationMatrix() const {
	return this->rotationMatrix;
}

std::vector<Mesh>& Model::getMeshes() {
	return this->meshes;
}

void Model::setPosition(glm::vec3 translation) {
	this->position = translation;

	this->aabbPosition = translation - this->worldToLocalPosition;

	this->updateModelMatrix();
}

void Model::setVelocity(glm::vec3 velocity) {
	this->velocity = velocity;
}

void Model::move(glm::vec3 translation) {
	this->position += translation;
	this->aabbPosition += translation;
	this->updateModelMatrix();
}

void Model::resize(glm::vec3 size) {
	this->size *= size;
	this->aabbSize *= size;
	this->updateModelMatrix();
}

void Model::setSize(glm::vec3 size) {
	this->size = size;
	this->aabbSize *= this->size;
	this->updateModelMatrix();
}

void Model::rotate(float angle, glm::vec3 direction) {
	this->rotation += direction * angle;

	rotation.x = rotation.x == 360 ? 0 : rotation.x;
	rotation.y = rotation.y == 360 ? 0 : rotation.y;
	rotation.z = rotation.z == 360 ? 0 : rotation.z;

	rotation.y = rotation.y == -90 ? 270 : rotation.y;
	rotation.y = rotation.y == -180 ? 180 : rotation.y;
	rotation.y = rotation.y == -270 ? 90 : rotation.y;

	this->updateModelMatrix();
}

void Model::setRotation(glm::vec3 rotation) {
	this->rotation = rotation;
	this->updateModelMatrix();
}

void Model::updateModelMatrix() {
	this->rotationMatrix = glm::rotate(glm::mat4(1), glm::radians(rotation.z), glm::vec3(0.0f, 0.0f, 1.0f));
	this->rotationMatrix = glm::rotate(this->rotationMatrix, glm::radians(rotation.y), glm::vec3(0.0f, 1.0f, 0.0f));
	this->rotationMatrix = glm::rotate(this->rotationMatrix, glm::radians(rotation.x), glm::vec3(1.0f, 0.0f, 0.0f));

	this->modelMatrix = glm::translate(glm::mat4(1.0f), this->position) * rotationMatrix * glm::scale(glm::mat4(1.0f), this->size);
}

void Model::generateAABB3DValues() {

	this->aabbPosition.x = INFINITY;
	this->aabbPosition.y = INFINITY;
	this->aabbPosition.z = INFINITY;

	this->aabbSize.x = -INFINITY;
	this->aabbSize.y = -INFINITY;
	this->aabbSize.z = -INFINITY;

	for (Mesh& m: this->meshes) {
		const std::vector<Vertex>& vertices = m.getVertices();
		
		for (const Vertex& v: vertices) {
			glm::vec3 worldPosition = this->modelMatrix * glm::vec4(v.position, 1.0f);
			
			if (worldPosition.x < this->aabbPosition.x)
				this->aabbPosition.x = worldPosition.x;
			if (worldPosition.y < this->aabbPosition.y)
				this->aabbPosition.y = worldPosition.y;
			if (worldPosition.z < this->aabbPosition.z)
				this->aabbPosition.z = worldPosition.z;

			if (worldPosition.x > this->aabbSize.x)
				this->aabbSize.x = worldPosition.x;
			if (worldPosition.y > this->aabbSize.y)
				this->aabbSize.y = worldPosition.y;
			if (worldPosition.z > this->aabbSize.z)
				this->aabbSize.z = worldPosition.z;
		}
	}
	this->aabbSize = this->aabbSize - this->aabbPosition;
	this->worldToLocalPosition = this->position - this->aabbPosition;
}

Model::Type Model::getType() const {
	return this->type;
}

void Model::setType(Model::Type type) {
	this->type = type;
}

const float Model::gravity = -30.0f;