#include "FileException.hpp"

FileException::FileException(const std::string& fileName, FileException::Type type) {
	this->type = type;
	this->fileName = fileName;
}

const char* FileException::what() const throw() {
	switch(this->type) {
	case FileException::NOT_FOUND:
		return std::string("File not found: " + this->fileName).c_str();
		break;
	case FileException::TEXTURE:
		return std::string("Failed to load texture: " + this->fileName).c_str();
		break;
	case FileException::FONT:
		return std::string("Failed to load font: " + this->fileName).c_str();
		break;
	case FileException::SHADER:
		return "Error while reading the shaders";
		break;
	case FileException::SHADER_ERROR:
		return std::string("Error in shader: " + this->fileName).c_str();
		break;
	case FileException::DEFAULT:
	default:
		return std::string("File Exception: " + this->fileName).c_str();
	}
}