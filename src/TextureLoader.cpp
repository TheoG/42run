#include "TextureLoader.hpp"
#include "FileException.hpp"

TextureLoader::TextureLoader() {}

TextureLoader::~TextureLoader() {}

unsigned int TextureLoader::textureFromFile(const std::string& fullPath) {
	unsigned int textureID;
	glGenTextures(1, &textureID);

	int width, height, nbComponents;

	unsigned char* data = stbi_load(fullPath.c_str(), &width, &height, &nbComponents, 0);

	if (data) {
		GLenum format;
		switch (nbComponents) {
		case 1:
			format = GL_RED;
			break;
		case 3:
			format = GL_RGB;
			break;
		case 4:
			format = GL_RGBA;
			// Enable Alpha and PNG Transparency
			glEnable(GL_BLEND);
			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
			break;
		}

		glBindTexture(GL_TEXTURE_2D, textureID);
		glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format, GL_UNSIGNED_BYTE, data);

		glGenerateMipmap(GL_TEXTURE_2D);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

		// Good Quality, may have some perfomance issues when too many texture on screen
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_LOD, 1.0f);

		stbi_image_free(data);
	}
	else {
		stbi_image_free(data);
		throw FileException(fullPath, FileException::Type::TEXTURE);
	}
	return textureID;
}